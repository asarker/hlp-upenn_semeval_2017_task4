import re
from nltk.stem.porter import *
stemmer = PorterStemmer()
from nltk.corpus import stopwords
import numpy as np


def lowercase_stem_and_remove_stopwords(list_of_texts):
    processed_text_list = []
    for raw_text in list_of_texts:
        try:
            raw_text = raw_text.decode('utf8')
            # Remove non-letters
            letters_only = re.sub("[^a-zA-Z]", " ", raw_text)
            # Convert to lower case, stem, and split into individual words
            words = [stemmer.stem(w) for w in letters_only.lower().split()]
            # In Python, searching a set is much faster than searching
            #   a list, so convert the stop words to a set
            stops = set(stopwords.words("english"))
            #Remove stop words
            meaningful_words = [w for w in words if not w in stops]
            processed_text = (" ".join(meaningful_words))
            processed_text_list.append(processed_text)
        except:
            processed_text_list.append(raw_text.lower())
    return processed_text_list

def lowercase_and_remove_stopwords(list_of_texts):
    processed_text_list = []
    for raw_text in list_of_texts:
        try:
            raw_text = raw_text.decode('utf8')
            # Remove non-letters
            letters_only = re.sub("[^a-zA-Z]", " ", raw_text)
            # Convert to lower case, stem, and split into individual words
            words = [w for w in letters_only.lower().split()]
            # In Python, searching a set is much faster than searching
            #   a list, so convert the stop words to a set
            stops = set(stopwords.words("english"))
            #Remove stop words
            meaningful_words = [w for w in words if not w in stops]
            processed_text = (" ".join(meaningful_words))
            processed_text_list.append(processed_text)
        except:
            processed_text_list.append(raw_text.lower())

    return processed_text_list

def get_embedding_vectors(list_of_texts, loaded_model):
    processed_embedding_list = []
    for text in list_of_texts:
        embedding = np.zeros(400)
        tokens = text.split()
        for token in tokens:
            try:
                token_vec = loaded_model[token]
                embedding = np.add(token_vec,embedding)
            except:
                pass
        processed_embedding_list.append(embedding)
    return processed_embedding_list

def get_train_test_inds(y,exp,train_proportion=0.8):
    y=np.array(y)
    train_inds = np.zeros(len(y),dtype=bool)
    test_inds = np.zeros(len(y),dtype=bool)
    values = np.unique(y)
    for value in values:
        value_inds = np.nonzero(y==value)[0]
        np.random.shuffle(value_inds)
        n = int(train_proportion*len(value_inds))

        train_inds[value_inds[:n]]=True
        test_inds[value_inds[n:]]=True
    #write them in file..
    '''
    outfile = open('./inds/train_inds_'+str(exp),'w')
    for i in train_inds:
        outfile.write(str(i)+'\t')
    outfile.close()
    outfile = open('./inds/test_inds_'+str(exp),'w')
    for i in test_inds:
        outfile.write(str(i)+'\t')
    outfile.close()
    '''
    return train_inds,test_inds



